"use strict";

/*
    Task 1:

    Напишите результат сравнения рядом с выражением в виде комментария.
    Необходимо выполнить это задание без использования console.log()
    Данные для сравнения:

    "42" == 42 true
    "0" == 0 true
    "0" == false true
    "true" == true false
    true == (1 == "1") true

    "42" === 42 false
    "0" === 0 false
    "0" === false false
    "true" === true false
    true === (1 === "1") false
*/
console.log("42" == 42);
console.log("0" == 0);
console.log("0" == false);
console.log("true" == true);
console.log(true == (1 == "1"));

console.log("42" === 42);
console.log("0" === 0);
console.log("0" === false);
console.log("true" === true);
console.log(true === (1 === "1"));




/* 
    Task 2:

    Переменная a = 'foo' , а переменная b = 'bar'
    Чему будет равен результат выражения: a+ +b 
    
    ps: два плюса - это не опечатка

*/

const a = 'foo';
const b = 'bar';
console.log(a+ +b);


/*
    Task 3:

    Напишите результат сравнения следующих выражений :

    "ананас" > "яблоко"
    undefined == null  
    undefined === null 

    Необходимо выполнить это задание без использования console.log()

*/
if("ананас" > "яблоко"){
    alert('Ананас больше чем яблоко');
} else if("ананас" < "яблоко"){
    alert('Ананас меньше чем яблоко');
} else{
    alert('что-то другое!');
}

if(undefined == null){
    alert('undefined == null');
} else if(undefined < null){
    alert('undefined < null');
} else{
    alert('undefined > null');
}

if(undefined === null){
    alert('undefined === null');
} else if(undefined < null){
    alert('undefined < null');
} else{
    alert('undefined > null');
}
/*
    Task 4:

    Создайте свою конструкцию УСЛОВИЕ
    Минимальное количество условий в одной конструкции: 5

*/
let weather = prompt('Какая погода на улице?', 'rainy, snowing, snowing, overcast');
if (weather == 'sunny') {
   alert('Сегодня хорошо и солнечно. Носите шорты! Идите на пляж, или в парк, и купите мороженое.');
  } else if (weather == 'rainy') {
    alert('Дождь падает за окном; возьмите плащ и зонт, и не находитесь слишком долго на улице.');
  } else if (weather == 'snowing') {
    alert('Снег падает - морозно! Лучше всего посидеть с чашкой горячего шоколада или слепить снеговика.');
  } else if (weather == 'overcast') {
    alert('Дождя нет, но небо серое и мрачное; он все может измениться в любую минуту, поэтому на всякий случай возьмите дождевик.');
  } else {
    alert('В моем списке нет ответа на указанный вами вариант!');
  }
/*
    Task 5:

    Созданную вами конструкцию из задания номер 4, запишите с помощью тернарного оператора

*/

/*
    Task 6:
    Загадайте 5 чисел от 1 до 100, 
    а затем запросите у пользователя одно число в этом диапазоне.
    При КАЖДОМ совпадении числа выводите сообщение в МОДАЛЬНОМ окне: 
    "Вы угадали число". Если пользователь не угадал, то выводите сообщение:
    "Вы не угадали. Попробуйте ещё раз!"

    В задании необходимо использовать конструкцию switch/case.
*/
// const numbers = []
// while (numbers.length < 5) {
//     const number = Math.round(Math.random() * 100)
//     if (!numbers.includes(number)) numbers.push(number)
// }

// const numb1 = numbers[0]
// const numb2 = numbers[1]
// const numb3 = numbers[2]
// const numb4 = numbers[3]
// const numb5 = numbers[4]
// console.log(numbers);

// let guessNumb = +prompt("Введите число:");
// switch(guessNumb) {
//     case numb1:
//         onSuccess();
//         break;
//     case numb2:
//         onSuccess();
//         break;
//     case numb3:
//         onSuccess();
//         break;
//     case numb4:
//         onSuccess();
//         break;
//     case numb5:
//         onSuccess();
//         break;
//     default:
//         alert("Вы не угадали. Попробуйте ещё раз!");
//         location.reload();
// }

// function onSuccess() {
//     alert("Вы угадали число");
//     console.log(guessNumb);
// }













// function guessNumber() {

//     let isNumber = function (n) {
//     return !isNaN(parseFloat(n)) && isFinite(n);
//     };

//     // Генератор случайного числа
//     const getRandomNumber = function(min, max) {
//     let rand = min - 0.5 + Math.random() * (max - min + 1);
//     return Math.round(rand);
//   } 

// const randomNumber = getRandomNumber(0,  100);
//   console.log('Загаданное число ' + randomNumber);
//     function game(tryNumber) {

//         if (tryNumber <= 0) alert("Ваши попытки закончились :(");
            
//         else {

//             let guess = prompt(`Угадайте число от 1 до 100. У вас осталось ${tryNumber} попыток`);
//             tryNumber--;

//             if ((guess > 0) && (guess <= 100) && (isNumber(guess))) {
    
//                 if (guess > randomNumber) {
//                     alert("Загаданное число меньше");
//                     game(tryNumber);
//                 }
//                 else if (guess < randomNumber) {
//                     alert("Загаданное число больше");
//                     game(tryNumber);
//                 }
//                 else if (+guess === randomNumber) {
//                     let again = confirm("Вы угадали! Хотите сыграть ещё раз?");
//                     if (again) guessNumber();
//                 }
//             }

//             else if (guess === null) {
//                 alert("Досвидания!");
//             }
        
//             else {
//                 alert("Угадайте ЧИСЛО от 1 до 100");
//                 game(tryNumber);
//             }
//         }
//     }
//     return game(5);
// }

// guessNumber();